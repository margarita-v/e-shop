#include "Methods.h"
#pragma once

typedef std::vector<int> marks;

class Rating
{
private:
	marks rating;
	marks::iterator FindMark(int pMark, bool &pFind);
public:
	Rating();
	~Rating();

	// ���������� ������
	void AddMark(int pMark);
	// �������������� ������
	void EditMark(int pFirstMark, int pSecondMark);
	// �������� ������
	void DeleteMark(int pMark);
	// ������� ��������
	int GetRating();
	
	bool IsEmpty();
};


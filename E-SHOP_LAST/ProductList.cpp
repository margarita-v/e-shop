#include "stdafx.h"
#include "ProductList.h"
#include <algorithm>

void PrintCategory(Category pCategory)
{
	switch (pCategory)
	{
	case 0: std::cout << "books\n"; break;
	case 1: std::cout << "games\n"; break;
	case 2: std::cout << "clothes\n"; break;
	case 3: std::cout << "music\n"; break;
	case 4: std::cout << "science\n"; break;
	case 5: std::cout << "studyng\n"; break;
	case 6: std::cout << "other\n"; break;
	}
}

Product* ProductList::FindByName(std::string pName)
{
	Trim(pName);
	bool pFind = false;
	prod_vect::iterator i = Products.begin();
	while ((!pFind) && i != Products.end())
	{
		pFind = (*i)->GetName() == pName;
		if (pFind)
			break;
		i++;
	}
	if (pFind)
	{
		Product* res = (*i);
		return res;
	}
	else
		return nullptr;
}

bool CheckName(std::string& name)
{
	DelSpaces(name);
	bool ok = name.size() > 0;
	return ok;
}
void Product::InputProduct(std::string pName)
{
	if (pName == "")
	{
		std::string str;
		std::cout << "������� �������� ������.\n";
		_flushall();
		std::getline(std::cin, str);
		while (!CheckName(str))
		{
			std::cout << "������! ������� �������� ��������.\n";
			std::cout << "������� �������� ������.\n";
			_flushall();
			std::getline(std::cin, str);
		}
		name = str;
	}
	else
		name = pName;
	InputDigit("������� ���� ������.", 1, 1000000, price);
	int num;
	InputDigit("������� ��������� ������ (����� �� 0 �� 6)\n0 - BOOKS\n1 - GAMES\n2 - CLOTHES\n3 - MUSIC\n4 - SCIENCE\n5 - STUDYNG\n6 - OTHER\n", 0, 6, num);
	category = Category(num);
	std::cout << "������� �������� ������.\n";
	std::getline(std::cin, description);
	InputDigit("������� ���������� ������.", 0, 1000000, count);
}
void Product::PrintProduct()
{
	std::cout << "������������ ������: " << name << std::endl;
	std::cout << "����: " << price << std::endl;
	std::cout << "���������: ";
	PrintCategory(category);
	std::cout << "��������: " << description << std::endl;
	std::cout << "����������: " << count << std::endl;
	std::cout << "������� ������: " << rating.GetRating() << std::endl;
	std::cout << "-------------------------------------------\n";
}

ProductList::ProductList()
{
	Products.reserve(0);
}

ProductList::~ProductList()
{
	int size = Products.size();
	for (int i = size - 1; i > 0; i--)
	{
		delete Products[i];
		Products.erase(Products.begin() + i);
	}
}

// ���������� ������
bool ProductList::Add(Product* p)
{
	bool find;
	std::string name = p->GetName();
	Trim(name);
	Product* el = FindByName(name);
	find = el != nullptr;
	if (!find)
		Products.push_back(p);
	return !find; // ���� ����� � ����� ������������� ��� ���� � ��������, �� �� �������� �� �����
}

// �������� ������
bool ProductList::Delete(std::string pName)
{
	Trim(pName);
	bool find = false;
	auto i = Products.begin();
	while ((!find) && i != Products.end())
	{
		find = (*i)->GetName() == pName;
		if (find)
			break;
		i++;
	}
	if (find)
		Products.erase(i);
	return find;
}
// �������������� ������
void ProductList::Edit(Product* p_pointer, const Product &p)
{
	bool ok = p_pointer != nullptr;
	if (ok)
	{
		std::string name = p.GetName();
		Trim(name);
		Rating r = p_pointer->GetRating();
		*p_pointer = p;
		(*p_pointer).SetRating(r);
	}
}
// ������ �������
void ProductList::Print()
{
	prod_vect::iterator i;
	if (Products.size())
		for (i = Products.begin(); i != Products.end(); ++i)
			(*i)->PrintProduct();
	else
		std::cout << "������ ������� ����." << std::endl;
}
// ����� ������ �� ���������
bool ProductList::FindByCategory(Category pCategory, prod_vect &result)
{
	prod_vect::iterator i;
	result.reserve(0);
	for (i = Products.begin(); i != Products.end(); ++i)
	{
		if ((*i)->GetCategory() == pCategory)
			result.push_back(*i);
	}
	return result.size() > 0;
}

// ����� ������� �� ��������� ������
bool ProductList::FindByMarks(double pMin, double pMax, prod_vect &result)
{
	if (pMax > pMin && pMin >= 1 && pMin <= 10 && pMax >= 1 && pMax <= 10)
	{
		prod_vect::iterator i;
		result.reserve(0);
		for (i = Products.begin(); i != Products.end(); ++i)
		{
			int r = (*i)->GetRating().GetRating();
			if (r && r >= pMin && r <= pMax)
				result.push_back(*i);
		}
		return result.size() > 0;
	}
	else
		throw std::bad_exception("�������� �������� ������.\n");
}

bool ProductList::FindByPrice(int pMin, int pMax, prod_vect &result)
{
	if (pMax > pMin && pMin >= 1 && pMin <= 1000000 && pMax >= 1 && pMax <= 1000000)
	{
		prod_vect::iterator i;
		result.reserve(0);
		for (i = Products.begin(); i != Products.end(); ++i)
		{
			int p = (*i)->GetPrice();
			if (p >= pMin && p <= pMax)
				result.push_back(*i);
		}
		return result.size() > 0;
	}
	else	
		throw std::bad_exception("�������� �������� ���.\n");
}

// ����� ��� N ������� � ������ �������� ��������
bool ProductList::FindBestMarks(int N, prod_vect &result)
{
	struct sort_class
	{
		bool operator() (Product* p1, Product* p2)
		{
			bool bigger = (p1->GetName() <= p2->GetName());
			int p1_rat = p1->GetRating().GetRating();
			int p2_rat = p2->GetRating().GetRating();
			return (!p1->GetRating().IsEmpty() && ((p1_rat > p2_rat) || ((p1_rat == p2_rat) && bigger)));
		}
	} sort_object;

	if (N > 0 && N <= Products.size())
	{
		std::sort(Products.begin(), Products.end(), sort_object);
		unsigned i = 0;
		for (auto it = Products.begin(); (it != Products.end()) && (i < N); ++it)
			if ((*it)->GetRating().GetRating())
			{
				result.push_back(*it);
				++i;
			}
		return result.size() != 0;
	}
	throw std::bad_exception("�������� �������� N.\n");
}
// ����� ��� N ������� � ������ ������� ��������
bool ProductList::FindWorstMarks(int N, prod_vect &result)
{
	struct sort_class
	{
		bool operator() (Product* p1, Product* p2)
		{
			bool bigger = (p1->GetName() >= p2->GetName());
			int p1_rat = p1->GetRating().GetRating();
			int p2_rat = p2->GetRating().GetRating();
			return (!p1->GetRating().IsEmpty() && ((p1_rat < p2_rat) || ((p1_rat == p2_rat) && bigger)));
		}
	} sort_object;

	if (N > 0 && N <= Products.size())
	{
		std::sort(Products.begin(), Products.end(), sort_object);
		unsigned i = 0;
		for (auto it = Products.begin(); (it != Products.end()) && (i < N); ++it)
			if ((*it)->GetRating().GetRating())
			{
				result.push_back(*it);
				++i;
			}
		return result.size() != 0;
	}
	throw std::bad_exception("�������� �������� N.\n");
}

#include "stdafx.h"
#include "Rating.h"

Rating::Rating()
{
	rating.reserve(0);
}


Rating::~Rating()
{
	rating.clear();
}

marks::iterator Rating::FindMark(int pMark, bool &pFind)
{
	pFind = false;
	marks::iterator i = rating.begin();
	while ((!pFind) && i != rating.end())
	{
		pFind = (*i) == pMark;
		if (pFind)
			break;
		i++;
	}
	if (pFind)
		return i;
}

// ���������� ������
void Rating::AddMark(int pMark)
{
	//if (pMark < 1 || pMark > 10)
		//throw std::bad_exception("�������� ������.");
	rating.push_back(pMark);
}

// �������������� ������
void Rating::EditMark(int pFirstMark, int pSecondMark)
{
	bool find;
	marks::iterator i = FindMark(pFirstMark, find);
	if (find)
	{
		rating.erase(i);
		AddMark(pSecondMark);
	}
}

// �������� ������
void Rating::DeleteMark(int pMark)
{
	bool find;
	marks::iterator i = FindMark(pMark, find);
	if (find)
		rating.erase(i);
}

// ������� ��������
int Rating::GetRating()
{
	int sum = 0;
	marks::iterator i;
	for (i = rating.begin(); i != rating.end(); ++i)
		sum += *i;
	if (sum)
		return sum / rating.size();
	else
		return 0;
}

bool Rating::IsEmpty()
{
	return rating.size() == 0;
}
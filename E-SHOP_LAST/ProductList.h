#include "Rating.h"
#pragma once
enum Category { BOOKS, GAMES, CLOTHES, MUSIC, SCIENCE, STUDYNG, OTHER };

void PrintCategory(Category pCategory);

class Product
{
	int price;
	std::string name;
	Category category;
	std::string description;
	int count;
	Rating rating;
public:
	Product() : price(0), name(""), category(OTHER), description(""), count(0), rating(Rating()) { }
	~Product() { }
	// ������������� ������
	void InputProduct(std::string pName = "");
	// ������ ������
	void PrintProduct();
	Category GetCategory() const { return category; }
	int GetCount() const { return count; }
	void SetCount(int value) 
	{ 
		if (value < 0)
			throw std::bad_exception("�������� ��������.");
		count = value; 
	}
	int GetPrice() const { return price; }
	Rating GetRating() const { return rating; }
	void SetRating(const Rating& r) { rating = r; }
	void AddMark(int mark)
	{
		if (mark < 0 || mark > 10)
			throw new std::bad_exception("�������� ������.");
		rating.AddMark(mark);
	}
	void EditMark(int first_mark, int second_mark)
	{
		if (second_mark < 0 || second_mark > 10)
			throw new std::bad_exception("�������� ������.");
		rating.EditMark(first_mark, second_mark);
	}
	void DelMark(int mark)
	{
		rating.DeleteMark(mark);
	}
	std::string GetName() const { return name; }
};

typedef std::vector<Product*> prod_vect;
typedef prod_vect::iterator elem;

class ProductList
{
private:
	/////friend class UserList;
	//friend struct Product;
	prod_vect Products;
public:
	ProductList();
	~ProductList();

	// ���������� ������
	bool Add(Product* p);
	// �������� ������
	bool Delete(std::string pName);
	// �������������� ������
	void Edit(Product* p_pointer, const Product &p); 
	// ������ �������
	void Print();
	// ����� ������ �� ���������
	bool FindByCategory(Category pCategory, prod_vect &result);
	// ����� ������� �� ��������� ������
	bool FindByMarks(double pMin, double pMax, prod_vect &result);
	// ����� ������� �� �������� ���������
	bool FindByPrice(int pMin, int pMax, prod_vect &result);
	// ����� ��� N ������� � ������ �������� ��������
	bool FindBestMarks(int N, prod_vect &result);
	// ����� ��� N ������� � ������ ������� ��������
	bool FindWorstMarks(int N, prod_vect &result);

	Product* FindByName(std::string pName);
	int Size() { return Products.size(); }
	void PrintNames(bool& was_printed) 
	{ 
		was_printed = false;
		for (auto i = Products.begin(); i != Products.end(); ++i)
			if ((*i)->GetCount() > 0)
			{
				std::cout << '\t' << (*i)->GetName() << std::endl;
				was_printed = true;
			}
	}
	void PrintFirstMark()
	{
		if ((*Products.begin())->GetRating().IsEmpty())
			std::cout << "� ������ ������ �� ����������." << std::endl;
		else
			(*Products.begin())->PrintProduct();
	}
};


#include "ProductList.h"
#pragma once
class purchased_products
{
	Product* prod;
	int mark;
	int count;
public:
	purchased_products() : prod(NULL), mark(0), count(0) { }
	int GetCount() const { return count; }
	void IncCount() { count++; }
	int GetMark() const { return mark; } 
	void SetMark(int value)
	{
		if (mark < 0 || mark > 10)
			throw std::bad_exception("�������� ������.");
		mark = value;
	}
	Product* GetProduct() { return prod; } 
	void SetProduct(Product* value)
	{
		if (value == NULL)
			throw std::bad_exception("������� ���������� �������� ���������.");
		prod = value;
	}
	void PrintInfo()
	{
		//std::cout << "\t������������ ������: " << prod->GetName() << std::endl;
		std::cout << "������: \n";
		if (mark != 0)
			std::cout << '\t' << mark << std::endl;
		else
			std::cout << '\t' << "������������ �� ������ ������ ������� ������." << std::endl;
		std::cout << "���������� �������: " << count << std::endl << std::endl;
	}
};

typedef std::vector<purchased_products*> Purchased_Products;
class User
{
	std::string nick;
	int age;
	std::string email;
	Purchased_Products products; // product and mark
public:
	User() : nick(""), age(1), email(""), products(Purchased_Products()) { }
	std::string GetNick() const { return nick; }
	int GetAge() const { return age; }
	std::string GetEmail() const { return email; }
	Purchased_Products GetProducts() { return products; }
	void SetProducts(const Purchased_Products& p) 
	{ 
		products.reserve(0);
		for (auto i = p.begin(); i != p.end(); ++i)
			products.push_back(*i);
	}
	void DelAllMarks()
	{
		if (products.size() > 0)
			for (auto i = products.begin(); i != products.end(); ++i)
				(*i)->GetProduct()->DelMark((*i)->GetMark());
	}
	void InputUser(std::string pNick = "", std::string pEmail = "");
	void PrintUser();
	// ������� ������
	void Buy(Product* prod);
	// �������� ������
	void DelMark(purchased_products* prod);
	// �������������� ������
	void EditMark(purchased_products* prod, int new_mark);

	void ChangeMark(purchased_products* prod, int mark = 0);

	void AddMark(purchased_products* prod, int new_mark);

	bool Find(std::string name, purchased_products* result);

	void DelProduct(std::string name)
	{
		Trim(name);
		int index = -1;
		int count = 0;
		for (auto i = products.begin(); (i != products.end()) && (index == -1); i++, count++)
			if ((*i)->GetProduct()->GetName() == name)
				index = count;
		if (index > -1)
			products.erase(products.begin() + index);
	}
	purchased_products* FindProduct(std::string name)
	{
		Trim(name);
		bool pFind = false;
		auto i = products.begin();
		while ((!pFind) && i != products.end())
		{
			pFind = (*i)->GetProduct()->GetName() == name;
			if (pFind)
				break;
			i++;
		}
		if (pFind)
		{
			purchased_products* res = (*i);
			return res;
		}
		else
			return nullptr;
	}
	void PrintProducts()
	{
		if (products.size())
			for (auto i = products.begin(); i != products.end(); ++i)
			{
				std::cout << "\t������������ ������: " << (*i)->GetProduct()->GetName() << std::endl;
				if ((*i)->GetMark())
					std::cout << "\t������ ������: " << (*i)->GetMark() << std::endl;
				else
					std::cout << "\t������ ������: �� �������" << std::endl;
				std::cout << "\t���������� �������: " << (*i)->GetCount() << std::endl;
				std::cout << "-------------------------------------------\n\n";
			}
		else
			std::cout << "������ ������������ ��� �� ����� �������.\n\n";
	}
	bool WasBought()
	{
		return products.size() > 0;
	}
};

typedef std::vector<User*> vect;

class UserList
{
private:
	vect Users;
public:
	UserList();
	~UserList();

	// ���������� ������������
	bool Add(User* user);
	// �������������� ������������
	void Edit(User* first, const User &second);
	// �������� ������������
	bool Delete(std::string pEmail);
	// ������ ������ ������������
	void Print();
	// ����� ������������ 
	User* FindByNickAndEmail(std::string pNick, std::string pEmail);
	User* FindByEmail(std::string pEmail);
	User* FindByNick(std::string pNick);
	// ����� ������������ �� ��������
	bool FindByAge(int pAge, vect &result);

	void DelProducts(std::string name)
	{
		Trim(name);
		for (auto i = Users.begin(); i != Users.end(); ++i)
			(*i)->DelProduct(name);
	}
};


// e-shop.cpp : Defines the entry point for the console application.
//
#include <locale>
#include <conio.h>
#include "stdafx.h"
#include "UserList.h"

using namespace std;

UserList Clients;
ProductList Shop;

void PrintMenu()
{
	cout << "\n�������� ��������� ��������.\n";
	cout << " 1 - ���������� ������\n";
	cout << " 2 - �������� ������\n";
	cout << " 3 - �������������� ������\n";
	cout << " --------------------------------------------------\n";
	cout << " 4 - ���������� ������������\n";
	cout << " 5 - �������� ������������\n";
	cout << " 6 - �������������� ������������\n";
	cout << " --------------------------------------------------\n";
	cout << " 7 - ������ ���������� � �������\n";
	cout << " 8 - ������ ���������� � �������������\n";
	cout << " --------------------------------------------------\n";
	cout << " 9 - ����� ������� �� ��������\n";
	cout << "10 - ����� ������� �� ���������\n";
	cout << "11 - ����� ������� �� �������� ���������\n";
	cout << "12 - ����� ������� �� �������� ������\n";
	cout << " --------------------------------------------------\n";
	cout << "13 - ����� ��� N ������� ������ �������� ��������\n";
	cout << "14 - ����� ��� N ������� ������ ������� ��������\n";
	cout << " --------------------------------------------------\n";
	cout << "15 - ����� ������������ �� ����\n";
	cout << "16 - ����� ������������ �� ��������\n";
	cout << " --------------------------------------------------\n";
	cout << "17 - ����� ������������ ��� ������������� �������\n";
	cout << "18 - ����� ������������ ��� �������������� ������\n";
	cout << "19 - ����� ������������ ��� �������� ������\n";
	cout << " 0 - �����\n";
}

bool InputQuery()
{
	string s;
	int m;
	bool correct = true;
	do
	{
		_flushall();
		getline(std::cin, s);
		Trim(s);
		correct = (s.size() == 1) && (s[0] == 'y' || s[0] == 'Y' || s[0] == '�' || s[0] == '�' || s[0] == 'n' || s[0] == 'N' || s[0] == '�' || s[0] == '�');
		if (!correct)
			cout << "������! ��������� ����.\n";
	} while (!correct);

	return (s[0] == 'y' || s[0] == 'Y' || s[0] == '�' || s[0] == '�');
}

void Dialog(string msg, bool del)
{
	string email, name;
	cout << "������� email ������������.\n";
	_flushall();
	getline(std::cin, email);
	User* user = Clients.FindByEmail(email);
	if (user == nullptr)
		cout << "������������ � ������ email �� ���������������." << endl;
	else
	{
		cout << "������ ��������� ������� ������������:\n";
		user->PrintProducts();
		/// ���� ������� ���� ���������
		if (user->WasBought())
		{
			// ������ ������������ ������, ������ �� ������� ��������� ��������
			cout << msg;
			_flushall();
			getline(std::cin, name);
			// ���� ������ �� ���������� ������������
			purchased_products* prod = user->FindProduct(name);
			if (prod == nullptr)
			{
				cout << "������ ����� ��� �� ��� ������ ������� �������������.\n";
			}
			else
			{
				prod->PrintInfo();
				if (prod->GetMark() == 0)
				{
					cout << "������� �������� ������? y/n \n";
					if (InputQuery())
					{
						int m;
						InputDigit("������� ����� ������ ������.", 1, 10, m);
						user->AddMark(prod, m);
					}
				}
				else // ���� ������ ����
				{
					if (del)
					{
						cout << "������� ������� ������? y/n \n";
						if (InputQuery())
							user->DelMark(prod);
					}
					else
					{
						int m;
						InputDigit("������� ����� ������ ������.", 1, 10, m);
						user->EditMark(prod, m);
					}
				}
			} // ����� ��� ������
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	Product prod;
	User user;
	int prod_count, users_count;
	InputDigit("������� ���������� �������.", 1, 20, prod_count);
	for (int i = 0; i < prod_count; i++)
	{
		cout << "������� ���������� � " << i + 1 << "-� ������.\n";
		Product* p = new Product();
		p->InputProduct();
		if (!Shop.Add(p))
			cout << "����� � ����� ��������� ��� ���� � ��������.\n" << endl;		
	}
	InputDigit("������� ���������� �������������.", 1, 20, users_count);
	for (int i = 0; i < users_count; i++)
	{
		cout << "������� ���������� � " << i + 1 << "-� ������������.\n";
		User* p = new User();
		p->InputUser();
		if (!Clients.Add(p))
			cout << "������������ � ����� ����� ��� email ��� ���������������." << endl;
	}
	int item, N;
	string name, email;
	PrintMenu();
	InputDigit("---> ", 0, 19, item);
	while (item)
	{
		switch (item)
		{
		case 1: // ���������� ������ X
		{
			do
			{
				std::cout << "������� ������������ ������.\n";
				_flushall();
				getline(std::cin, name);
				//Trim(name);
				DelSpaces(name);
			} while (name.size() <= 0);
			Product* p = Shop.FindByName(name);
			if (p == nullptr) // ���� ����� � ����� ������������� �� ������
			{
				p = new Product();
				p->InputProduct(name);
				Shop.Add(p);
				cout << "����� ��� ��������.\n";
			}
			else // ����� � ����� ������������� ������
			{
				cout << "����� � ����� ��������� ��� ���� � ��������. �������� ��� ����������? y/n" << endl;
				if (InputQuery())
				{
					int count;
					InputDigit("������� ���������� ������.", 1, 1000000, count);
					p->SetCount(count);
				}
			}
			break;
		}
		case 2: // �������� ������ X
		{
			cout << "������� ������������ ������.\n";
			_flushall();
			getline(std::cin, name);
			//Trim(name);
			DelSpaces(name);
			if (!Shop.Delete(name))
				cout << "������ � ������ ������������� � �������� �� �������.\n";
			else
			{
				Clients.DelProducts(name);
				cout << "����� ��� ������.\n";
			}
			break;
		}
		case 3: // �������������� ������ X
		{
			cout << "������� ������������ ������.\n";
			_flushall();
			getline(std::cin, name);
			//Trim(name);
			DelSpaces(name);
			Product* p_first = Shop.FindByName(name);
			if (p_first == nullptr) // �����, ������� ��������� ��������, �� ������
				cout << "������ � ������ ������������� � �������� �� �������.\n";
			else
			{	// ����� ������
				cout << "������� ����� ������������ ������.\n";
				_flushall();
				getline(std::cin, name);
				//Trim(name);
				DelSpaces(name);
				Product* p_second = Shop.FindByName(name);
				// ���� ����� � ����� ������������� ��� ����
				if (p_second != nullptr && p_second != p_first)
					cout << "����� � ����� ������������� ��� ���� � ��������.\n";
				else
				{	// ����� � ����� ������������� �� ������ ����� ������������� ��������
					p_second = new Product();
					p_second->InputProduct(name);
					Shop.Edit(p_first, *p_second);
				}
			}
			break;
		}
		//------------------------------------------------------------
		case 4: // ���������� ������������ X
		{
			User* p = new User();
			p->InputUser();
			if (!Clients.Add(p))
				cout << "������������ � ����� ����� ��� email ��� ���������������." << endl;
			else
				cout << "������������ ��� ��������.\n";
			break;
		}
		case 5: // �������� ������������ X
		{
			cout << "������� email ������������.\n";
			_flushall();
			getline(std::cin, email);
			Trim(email);
			if (!Clients.Delete(email))
				cout << "������������ � ����� email �� ���������������.\n";
			else
				cout << "������������ ��� ������.\n";
			// ������� ������������ ������
			break;
		}
		case 6: // �������������� ������������ X
		{
			cout << "������� email ������������.\n";
			_flushall();
			getline(std::cin, email);
			Trim(email);
			User* first = Clients.FindByEmail(email);
			if (first == nullptr)
				cout << "������������ � ����� email �� ���������������.\n";
			else
			{
				// ������������ ������
				cout << "������� ����� email ������������.\n";
				_flushall();
				getline(std::cin, email);
				Trim(email);
				cout << "������� ��� ������������.\n";
				_flushall();
				getline(std::cin, name);
				Trim(name);
				User* second = Clients.FindByNickAndEmail(name, email);
				// ���� ������������ � ����� email ��� ����
				if (second != nullptr && second != first)
					cout << "������������ � ����� ����� ��� email ��� ���������������.\n";
				else
				{	
					second = new User();
					second->InputUser(name, email);
					Clients.Edit(first, *second);
				}
			}
			break;
		}
		//------------------------------------------------------------
		case 7: // ������ ���������� � ������� X
		{
			Shop.Print();
			break;
		}
		case 8: // ������ ���������� � ������������� X
		{
			Clients.Print();
			break;
		}
		//------------------------------------------------------------
		case 9: // ����� ������ �� �������� X
		{
			cout << "������� ������������ ������.\n";
			_flushall();
			getline(std::cin, name);
			DelSpaces(name);
			Product* p = Shop.FindByName(name);
			if (p == nullptr)
				cout << "������ ����� ����������� � ��������.\n";
			else
				p->PrintProduct();
			break;
		}
		case 10: // ����� ������ �� ��������� X
		{
			int num;
			InputDigit("������� ��������� ������ (����� �� 0 �� 6)\n0 - BOOKS\n1 - GAMES\n2 - CLOTHES\n3 - MUSIC\n4 - SCIENCE\n5 - STUDYNG\n6 - OTHER\n", 0, 6, num);
			Category category = Category(num);
			prod_vect vect;
			if (!Shop.FindByCategory(category, vect))
				cout << "������� ������ ��������� �� �������." << endl;
			else
				for (auto i = vect.begin(); i != vect.end(); ++i)
					(*i)->PrintProduct();

			break;
		}
		case 11: // ����� ������ �� �������� ��������� X
		{
			int min, max;
			InputDigit("������� ������ ���� ���������.", 1, 999999, min);
			if (min == 999999)
				max = 1000000;
			else
				InputDigit("������� ������ ���� ���������.", min + 1, 1000000, max);
			prod_vect vect;
			if (!Shop.FindByPrice(min, max, vect))
				cout << "������� � ������ ������� ��������� �� �������." << endl;
			else
				for (auto i = vect.begin(); i != vect.end(); ++i)
					(*i)->PrintProduct();
			break;
		}
		case 12: // ����� ������ �� ��������� ������ 
		{
			int min, max;
			InputDigit("������� ������ ������ ���������.", 1, 9, min);
			if (min == 9)
			{
				max = 10;
				cout << "������ �������� ������ �� 9 �� 10.\n";
			}
			else
				InputDigit("������� ������ ������ ���������.", min + 1, 10, max);
			prod_vect vect;
			if (!Shop.FindByMarks(min, max, vect))
				cout << "������� � ������ ��������� ������ �� �������." << endl;
			else
				for (auto i = vect.begin(); i != vect.end(); ++i)
					(*i)->PrintProduct();
			break;
		}
		//------------------------------------------------------------
		case 13: // ����� ��� N ������� ������ �������� ��������
		{
			if (Shop.Size() == 0)
				cout << "������� ����." << endl;
			else
				if (Shop.Size() == 1)
					Shop.PrintFirstMark();
				else
				{
					InputDigit("������� ����� N.", 1, Shop.Size(), N);
					prod_vect vect;
					if (!Shop.FindBestMarks(N, vect))
						cout << "� ������� ������ �� ����������." << endl;
					else
						for (auto i = vect.begin(); i != vect.end(); ++i)
							(*i)->PrintProduct();
				}
			break;
		}
		case 14: // ����� ��� N ������� ������ ������� �������� 
		{
			if (Shop.Size() == 0)
				cout << "������� ����." << endl;
			else
				if (Shop.Size() == 1)
					Shop.PrintFirstMark();
				else
				{
					InputDigit("������� ����� N.", 1, Shop.Size(), N);
					prod_vect vect;
					if (!Shop.FindWorstMarks(N, vect))
						cout << "� ������� ������ �� ����������." << endl;
					else
						for (auto i = vect.begin(); i != vect.end(); ++i)
							(*i)->PrintProduct();
				}
			break;
		}
		//------------------------------------------------------------
		case 15: // ����� ������������ �� ���� X
		{
			cout << "������� ��� ������������.\n";
			_flushall();
			getline(std::cin, name);
			//Trim(name);
			DelSpaces(name);
			User* user = Clients.FindByNick(name); // ���������
			if (user == nullptr)
				cout << "������������ � ������ ����� �� ���������������." << endl;
			else
				user->PrintUser();
			break;
		}
		case 16: // ����� ������������ �� �������� X
		{
			InputDigit("������� ������� ������������.", 1, 100, N);
			vect users;
			if (!Clients.FindByAge(N, users))
				cout << "������������ ������ �������� �� ����������������." << endl;
			else
				for (auto i = users.begin(); i != users.end(); ++i)
					(*i)->PrintUser();
			break;
		}
		//------------------------------------------------------------------------------------------------------------------
		case 17: // ����� ������������ ��� ������������� ������� X
		{
			cout << "������� email ������������.\n";
			_flushall();
			getline(std::cin, email);
			Trim(email);
			User* user = Clients.FindByEmail(email);
			if (user == nullptr)
				cout << "������������ � ������ email �� ���������������." << endl;
			else
			{
				cout << "�������� �����, ������� ������ ������.\n��������� ������:\n";
				bool was_printed;
				Shop.PrintNames(was_printed);
				if (was_printed)
				{
					cout << "������� ������������ ���������� ������.\n";
					_flushall();
					getline(std::cin, name);
					Trim(name);
					Product* prod = Shop.FindByName(name);
					if (prod == nullptr)
						cout << "������ ����� ����������� � ��������.\n";
					else
					{
						if (prod->GetCount() == 0)
							cout << "������ ����� ���������� � ��������.\n";
						else
						{
							user->Buy(prod);
							purchased_products* prod = user->FindProduct(name);
							if (prod->GetMark() == 0)
							{
								int m;
								cout << "������� ��������� ������ ������? y/n ---> ";
								if (InputQuery())
								{
									InputDigit("������� ������ ������.", 1, 10, m);
									user->AddMark(prod, m);
								}
							}
						}
					}
				}
				else
					cout << "������ � �������� �����������.\n";
			}
			break;
		}
		//------------------------------------------------------------------------------------------------------------------
		case 18: // ����� ������������ ��� �������������� ������ X
		{
			Dialog("������� ������������ ������, ������ �� ������� ��������� ��������.\n", false);
			break;
		}
		//------------------------------------------------------------------------------------------------------------------
		case 19: // ����� ������������ ��� �������� ������ X
		{
			Dialog("������� ������������ ������, ������ �� ������� ��������� �������.\n", true);
			break;
		}
		case 20:
		{
			PrintMenu();
			break;
		}
		}
		//PrintMenu();
		InputDigit("---> ", 0, 20, item);
	}
	return 0;
}


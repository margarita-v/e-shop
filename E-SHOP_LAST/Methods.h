#include <vector>
#include <string>
#include <iostream>
#pragma once

bool IsANumber(char *num);
void InputDigit(std::string pMessage, int pMin, int pMax, int &pDigit);
void Trim(std::string &str);
void DelSpaces(std::string &str);
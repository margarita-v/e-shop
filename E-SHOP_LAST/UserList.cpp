#include "stdafx.h"
#include "UserList.h"

UserList::UserList()
{
	Users.reserve(0);
}
UserList::~UserList()
{
	int size = Users.size();
	for (int i = size - 1; i > 0; i--)
	{
		delete Users[i];
		Users.erase(Users.begin() + i);
	}
}

bool CheckEmail(std::string email)
{
	std::string str1, str2, str3;
	for (int i = 0; i < email.size(); i++)
		if (email[i] >= '�' && email[i] <= '�' || email[i] >= '�' && email[i] <= '�')
			return false;

	if (email.size() == 0)
		return false;

	Trim(email);

	// email ������ ���������� � �����
	bool ok = email[0] >= 'A' && email[0] <= 'Z' || email[0] >= 'a' && email[0] <= 'z';
	if (ok)
	{
		// email ������ ����� 1 ������ @
		int count = 0;
		for (int i = 0; i != email.size(); ++i)
			if (email[i] == '@')
				count++;
		ok = count == 1;
		if (ok)
		{
			// email ������ ����� 1 ������ .
			count = 0;
			for (int i = 0; i != email.size(); ++i)
				if (email[i] == '.')
					count++;
			ok = count == 1;
			if (ok)
			{
				// ��������� email �� 3 ���������
				count = 0;
				auto i = 0;
				for (; email[i] != '@'; ++i)
					str1.push_back(email[i]);
				i++;

				ok = str1.size() > 0;
				for (; email[i] != '.'; ++i)
					str2.push_back(email[i]);
				i++;

				for (; i != email.size(); ++i)
					str3.push_back(email[i]);

				ok = str1.size() > 0 && str2.size() > 0 && str3.size() > 0;

				if (ok)
				{
					// � str1 ����� ���� ����� � �����
					for (int i = 0; ok && (i != str1.size()); ++i)
						ok = isalpha(str1[i]) || isdigit(str1[i]);

					if (ok)
					{
						// � str2 ����� ���� ������ �����
						for (int i = 0; ok && (i != str2.size()); ++i)
							ok = isalpha(str2[i]);
						if (ok)
						{
							// � str3 ����� ���� ������ �����
							for (int i = 0; ok && (i != str3.size()); ++i)
								ok = isalpha(str3[i]);
						}
					}
				}
			}
		}
	}
	return ok;
}
bool CheckNick(std::string& nick)
{
	DelSpaces(nick);
	bool ok = nick.size() > 0;
	return ok;
}
void User::InputUser(std::string pNick, std::string pEmail)
{
	if (pNick == "" || pEmail == "")
	{
		std::string str;
		std::cout << "������� ���.\n";
		_flushall();
		std::getline(std::cin, str);
		while (!CheckNick(str))
		{
			std::cout << "������! ������ �������� ���.\n";
			_flushall();
			std::getline(std::cin, str);
		}
		nick = str;
		std::cout << "������� email.\n";
		std::getline(std::cin, str);
		_flushall();
		while (!CheckEmail(str))
		{
			std::cout << "������ �������� email. ��������� ����.\n";
			std::cout << "������� email.\n";
			std::getline(std::cin, str);
		}
		email = str;
	}
	else
	{
		nick = pNick;
		email = pEmail;
	}
	InputDigit("������� �������. ", 1, 100, age);
}
void User::PrintUser()
{
	std::cout << "���: " << nick << std::endl;
	std::cout << "�������: " << age << std::endl;
	std::cout << "Email: " << email << std::endl;
	std::cout << "-------------------------------------------\n";
	std::cout << "������ ��������� �������:\n";
	PrintProducts();
}

bool User::Find(std::string name, purchased_products* result)
{
	Trim(name);
	result = FindProduct(name);
	return result != nullptr;
}
// ������� ������
void User::Buy(Product* prod)
{
	purchased_products* product = FindProduct(prod->GetName());
	int c = prod->GetCount();
	prod->SetCount(--c);
	
	if (product == nullptr) // ���� ������� ��� �� ��� ������
	{
		purchased_products* new_prod = new purchased_products();
		new_prod->SetProduct(prod);
		new_prod->IncCount();
		products.push_back(new_prod);
	}
	else
		product->IncCount();
}

void User::ChangeMark(purchased_products* prod, int mark)
{
	if (mark)
	{
		prod->GetProduct()->EditMark(prod->GetMark(), mark);
		prod->SetMark(mark);
	}
	else
	{
		prod->GetProduct()->DelMark(prod->GetMark());
		prod->SetMark(0);
	}
}
// �������� ������
void User::DelMark(purchased_products* prod)
{
	prod->GetProduct()->DelMark(prod->GetMark());
	prod->SetMark(0);
}

// �������������� ������
void User::EditMark(purchased_products* prod, int new_mark)
{
	prod->GetProduct()->EditMark(prod->GetMark(), new_mark);
	prod->SetMark(new_mark);
}

void User::AddMark(purchased_products* prod, int new_mark)
{
	prod->GetProduct()->AddMark(new_mark);
	prod->SetMark(new_mark);
}

// ����� ������������ 
User* UserList::FindByNickAndEmail(std::string pNick, std::string pEmail)
{
	Trim(pNick);
	Trim(pEmail);
	bool pFind = false;
	vect::iterator i = Users.begin();
	while ((!pFind) && i != Users.end())
	{
		pFind = ((*i)->GetNick() == pNick) || ((*i)->GetEmail() == pEmail);
		if (pFind)
			break;
		i++;
	}
	if (pFind)
	{
		User* res = (*i);
		return res;
	}
	else
		return nullptr;
}
User* UserList::FindByEmail(std::string pEmail)
{
	Trim(pEmail);
	bool pFind = false;
	vect::iterator i = Users.begin();
	while ((!pFind) && i != Users.end())
	{
		pFind = ((*i)->GetEmail() == pEmail);
		if (pFind)
			break;
		i++;
	}
	if (pFind)
	{
		User* res = (*i);
		return res;
	}
	else
		return nullptr;
}
// ����� ������������ �� ����
User* UserList::FindByNick(std::string pNick)
{
	Trim(pNick);
	vect::iterator i;
	for (i = Users.begin(); i != Users.end(); ++i)
	{
		if ((*i)->GetNick() == pNick)
			return *i;
	}
	return nullptr;
}
// ���������� ������������
bool UserList::Add(User* user)
{
	bool find;
	std::string nick = user->GetNick();
	std::string email = user->GetEmail();
	Trim(nick);
	Trim(email);
	User* p = FindByNickAndEmail(nick, email);
	find = p != nullptr;
	if (!find)
		Users.push_back(user);
	return !find;
}
// �������������� ������������
void UserList::Edit(User* first, const User &second)
{
	bool ok = first != nullptr;
	if (ok)
	{
		std::string nick = second.GetNick();
		std::string email = second.GetEmail();
		Trim(nick);
		Trim(email);
		Purchased_Products p = first->GetProducts();
		*first = second;
		(*first).SetProducts(p);
	}
}
// �������� ������������
bool UserList::Delete(std::string pEmail)
{
	Trim(pEmail);
	bool find = false;
	auto i = Users.begin();
	while ((!find) && i != Users.end())
	{
		find = (*i)->GetEmail() == pEmail;
		if (find)
			break;
		i++;
	}
	if (find)
	{
		(*i)->DelAllMarks();
		delete (*i);
		Users.erase(i);
	}
	return find;
}
// ������ ������ ������������
void UserList::Print()
{
	vect::iterator i;
	int num = 0;
	if (Users.size())
		for (i = Users.begin(); i != Users.end(); ++i, num++)
			(*i)->PrintUser();
	else
		std::cout << "������ ������������� ����." << std::endl;
}
// ����� ������������ �� ��������
bool UserList::FindByAge(int pAge, vect &result)
{
	vect::iterator i;
	result.reserve(0);
	for (i = Users.begin(); i != Users.end(); ++i)
	{
		if ((*i)->GetAge() == pAge)
			result.push_back(*i);
	}
	return result.size() > 0;
}
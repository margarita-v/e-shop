#include "stdafx.h"
#include "Methods.h"

bool IsANumber(std::string s)
{
	int i;
	int len = s.length();
	for (i = 0; i < len && isdigit((unsigned char)s[i]); i++);
	return (len) && (i == len);
}

void Trim(std::string &str)
{
	const auto strBegin = str.find_first_not_of(" \t");
	if (strBegin != std::string::npos)
	{
		const auto strEnd = str.find_last_not_of(" \t");
		const auto strRange = strEnd - strBegin + 1;
		str = str.substr(strBegin, strRange);
	}
	else
		str = "";

	std::string result = "";
	for (unsigned int i = 0; i < str.length(); i++)
		result += (unsigned char)tolower(str.at(i)); 
	str = result;
}
void DelSpaces(std::string &str)
{
	std::string result = "";
	Trim(str);
	unsigned i = 0;
	while (i < str.length())
	{
		while (str[i] != ' ' && i < str.length())
		{
			result.push_back(str[i]);
			i++;
		}
		if (str[i] == ' ' && i < str.length())
			result.push_back(' ');
		while (str[i] == ' ' && i < str.length())
			i++;
	}
	str = result;
}

bool CheckDigit(std::string& str)
{
	Trim(str);
	bool ok = str.size() > 0;
	return ok;
}
void InputDigit(std::string pMessage, int pMin, int pMax, int &pDigit)
{
	std::string num;
	bool correct = false;
	std::cout << pMessage << std::endl;
	_flushall();
	std::getline(std::cin, num);
	correct = CheckDigit(num) && IsANumber(num);
	if (correct)
	{
		pDigit = stoi(num);
		correct = (pDigit >= pMin && pDigit <= pMax);
	}
	_flushall();
	while (!correct)
	{
		std::cout << "������! ������� ����� �� " << pMin << " �� " << pMax << std::endl;
		_flushall();
		std::getline(std::cin, num);
		correct = CheckDigit(num) && IsANumber(num);
		if (correct)
		{
			pDigit = stoi(num);
			correct = (pDigit >= pMin && pDigit <= pMax);
		}
		_flushall();
	}
}